﻿using EscolaPenseDiferente.Core.Entities;
using EscolaPenseDiferente.Core.Interfaces.IRepository;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace EscolaPenseDiferente.Infra.Repository
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {

        private readonly IMemoryCache _memoryCache;

        private const string KEY_CACHE_ALUNOS = "KEY_ALUNOS";
        private const string KEY_CACHE_ESCOLA = "KEY_ESCOLA";

        public BaseRepository(IMemoryCache memoryCache)
        {
            this._memoryCache = memoryCache;
        }

        public async Task<List<T>> GetAll(string KEY_CACHE)
        {
            if (_memoryCache.TryGetValue(KEY_CACHE, out List<T> list))
            {
                return list;
            }

            return null;
        }

        public async Task<List<T>> GetAll(string KEY_CACHE, int page, int pageSize)
        {
            if (pageSize <= 0)
                pageSize = 10;

            var skip = 0;
            if (page <= 0 || page == 1) { skip = 0; } else { skip = pageSize * (page - 1); }

            if (_memoryCache.TryGetValue(KEY_CACHE, out List<T> list))
            {
                if (list == null || list.Count == 0)
                    return null;

                return (list.Count > 0) ? list.Skip(skip).Take(pageSize).ToList() : null;
            }

            return null;
        }

        public async Task<Alunos> GetByIdAluno(string KEY_ALUNO, Guid id)
        {
            if (_memoryCache.TryGetValue(KEY_ALUNO, out List<Alunos> list))
            {
                var listObject = (list.Count > 0) ? list.ToList() : null;

                if (listObject == null)
                    return null;

                var objectAluno = listObject.Where(x => x.iCodAluno == id).FirstOrDefault();

                return objectAluno;
            }

            return null;
        }

        public async Task<Escola> GetByIdEscola(string KEY_ESCOLA, Guid id)
        {
            if (_memoryCache.TryGetValue(KEY_ESCOLA, out List<Escola> list))
            {
                var listObject = (list.Count > 0) ? list.ToList() : null;

                if (listObject == null)
                    return null;

                var objectEscola = listObject.Where(x => x.iCodEscola == id).FirstOrDefault();

                return objectEscola;
            }

            return null;
        }

        public async Task<bool> SaveChangesAlunos(string KEY_ALUNOS, List<Alunos> listAlunos, Alunos newAluno)
        {
            try
            {
                var memoryCacheEntryOptions = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(3600),
                    SlidingExpiration = TimeSpan.FromSeconds(1200)
                };

                newAluno.iCodAluno = Guid.NewGuid();

                if (listAlunos == null)
                {
                    listAlunos = new List<Alunos>();
                }

                listAlunos.Add(newAluno);

                _memoryCache.Set(KEY_ALUNOS, listAlunos, memoryCacheEntryOptions);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> SaveChangesEscola(string KEY_ESCOLA, List<Escola> listEscola, Escola newEscola)
        {
            try
            {
                var memoryCacheEntryOptions = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(3600),
                    SlidingExpiration = TimeSpan.FromSeconds(1200)
                };

                newEscola.iCodEscola = Guid.NewGuid();

                if(listEscola == null)
                {
                    listEscola = new List<Escola>();
                }

                listEscola.Add(newEscola);

                _memoryCache.Set(KEY_ESCOLA, listEscola, memoryCacheEntryOptions);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> UpDateAlunos(string KEY_ALUNOS, List<Alunos> listAlunos, Alunos updateAlunos)
        {
            try
            {
                if (listAlunos == null)
                {
                    return false;
                }

                var memoryCacheEntryOptions = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(3600),
                    SlidingExpiration = TimeSpan.FromSeconds(1200)
                };

                List<Alunos> newListAlunos = new List<Alunos>();

                foreach (var eachAlunos in listAlunos)
                {
                    if (updateAlunos.iCodAluno == eachAlunos.iCodAluno)
                    {
                        newListAlunos.Add(updateAlunos);
                    }
                    else
                    {
                        newListAlunos.Add(eachAlunos);
                    }
                }

                _memoryCache.Set(KEY_ALUNOS, newListAlunos, memoryCacheEntryOptions);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> UpDateEscola(string KEY_ESCOLA, List<Escola> listEscola, Escola updateEscola)
        {
            try
            {
                if(listEscola == null)
                {
                    return false;
                }

                var memoryCacheEntryOptions = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(3600),
                    SlidingExpiration = TimeSpan.FromSeconds(1200)
                };

                List<Escola> newListEscola = new List<Escola>();

                foreach (var eachEscola in listEscola)
                {
                    if(updateEscola.iCodEscola == eachEscola.iCodEscola)
                    {
                        newListEscola.Add(updateEscola);
                    }
                    else
                    {
                        newListEscola.Add(eachEscola);
                    }
                }

                _memoryCache.Set(KEY_ESCOLA, newListEscola, memoryCacheEntryOptions);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> DeleteAlunos(string KEY_ALUNOS, List<Alunos> listAlunos, Alunos deleteAlunos)
        {
            try
            {
                if (listAlunos == null)
                {
                    return false;
                }

                var memoryCacheEntryOptions = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(3600),
                    SlidingExpiration = TimeSpan.FromSeconds(1200)
                };

                List<Alunos> newListAlunos = new List<Alunos>();

                foreach (var eachAlunos in listAlunos)
                {
                    if (deleteAlunos.iCodAluno != eachAlunos.iCodAluno)
                    {
                        newListAlunos.Add(eachAlunos);
                    }
                }

                _memoryCache.Set(KEY_ALUNOS, newListAlunos, memoryCacheEntryOptions);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> DeleteEscola(string KEY_ESCOLA, List<Escola> listEscola, Escola deleteEscola)
        {
            try
            {
                var memoryCacheEntryOptions = new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(3600),
                    SlidingExpiration = TimeSpan.FromSeconds(1200)
                };

                List<Escola> newListEscola = new List<Escola>();

                foreach (var eachEscola in listEscola)
                {
                    if (deleteEscola.iCodEscola != eachEscola.iCodEscola)
                    {
                        newListEscola.Add(eachEscola);
                    }
                }

                _memoryCache.Set(KEY_ESCOLA, newListEscola, memoryCacheEntryOptions);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
