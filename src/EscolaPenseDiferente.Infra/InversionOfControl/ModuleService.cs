﻿using EscolaPenseDiferente.Core.Interfaces.IServices;
using EscolaPenseDiferente.Core.Services;
using Microsoft.Extensions.DependencyInjection;

namespace EscolaPenseDiferente.Infra.InversionOfControl
{
    public static class ModuleService
    {
        public static void startService(this IServiceCollection service)
        {
            service.AddScoped(typeof(IBaseService<>), typeof(BaseService<>));

            service.AddScoped<IAlunosService, AlunosService>();
            service.AddScoped<IEscolaService, EscolaService>();
        }
    }
}
