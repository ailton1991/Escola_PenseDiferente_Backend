﻿using EscolaPenseDiferente.Core.Interfaces.IRepository;
using EscolaPenseDiferente.Infra.Repository;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscolaPenseDiferente.Infra.InversionOfControl
{
    public static class ModuleRepository
    {
        public static void startRepository(this IServiceCollection service)
        {
            service.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));

            service.AddScoped<IAlunosRepository, AlunosRepository>();
            service.AddScoped<IEscolaRepository, EscolaRepository>();
        }
    }
}
