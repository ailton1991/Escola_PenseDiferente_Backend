﻿using EscolaPenseDiferente.Api.DTO;
using EscolaPenseDiferente.Core.Entities;
using EscolaPenseDiferente.Core.Interfaces.IServices;
using EscolaPenseDiferente.Core.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EscolaPenseDiferente.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AlunosController : ControllerBase
    {

        private const string KEY_CACHE_ALUNOS = "KEY_ALUNOS";
        private const string KEY_CACHE_ESCOLA = "KEY_ESCOLA";

        private readonly IAlunosService _alunoService;
        private readonly IEscolaService _escolaService;
        private readonly ILogger<AlunosController> _logger;

        public AlunosController
        (
            ILogger<AlunosController> logger,
            IAlunosService alunoService,
            IEscolaService escolaService
        )
        {
            _logger = logger;
            _alunoService = alunoService;
            _escolaService = escolaService;
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            responseAlunosDTO _responseAlunosCoreDTO = new responseAlunosDTO();

            try
            {
                var alunosEntidade = await _alunoService.GetAll(KEY_CACHE_ALUNOS);
                var escolaEntidade = await _escolaService.GetAll(KEY_CACHE_ESCOLA);

                if (alunosEntidade == null)
                {
                    _responseAlunosCoreDTO.IsSuccess = false;
                    _responseAlunosCoreDTO.Message = "Não encontramos nenhum(a) aluno(a).";
                    return Ok(_responseAlunosCoreDTO);
                }

                var retornoJoinEscolaPorListaAluno = await _escolaService.JoinEscolaPorListaAluno(alunosEntidade, escolaEntidade);

                _responseAlunosCoreDTO.IsSuccess = true;
                _responseAlunosCoreDTO.Message = "Sucesso";
                _responseAlunosCoreDTO.listaObjetosAlunos = retornoJoinEscolaPorListaAluno;

                return Ok(_responseAlunosCoreDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro: ", ex.Message);
                return BadRequest("Erro: " + ex.Message);
            }
        }

        [HttpGet]
        [Route("GetAllPage")]
        public async Task<IActionResult> GetAllPage(int page, int pageSize)
        {
            responseAlunosDTO _responseAlunosCoreDTO = new responseAlunosDTO();

            try
            {
                var alunosEntidade = await _alunoService.GetAll(KEY_CACHE_ALUNOS, page, pageSize);
                var escolaEntidade = await _escolaService.GetAll(KEY_CACHE_ESCOLA);

                if (alunosEntidade == null)
                {
                    _responseAlunosCoreDTO.IsSuccess = false;
                    _responseAlunosCoreDTO.Message = "Não encontramos nenhum resultado.";
                    return Ok(_responseAlunosCoreDTO);
                }

                var retornoJoinEscolaPorListaAluno = await _escolaService.JoinEscolaPorListaAluno(alunosEntidade, escolaEntidade);

                _responseAlunosCoreDTO.IsSuccess = true;
                _responseAlunosCoreDTO.Message = "Sucesso";
                _responseAlunosCoreDTO.listaObjetosAlunos = retornoJoinEscolaPorListaAluno;

                return Ok(_responseAlunosCoreDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro: ", ex.Message);
                return BadRequest("Erro: " + ex.Message);
            }
        }

        [HttpGet]
        [Route("GetById")]
        public async Task<IActionResult> GetById(string iCodAluno)
        {
            responseAlunosDTO _responseAlunosCoreDTO = new responseAlunosDTO();

            try
            {
                if (!Guid.TryParse(iCodAluno, out var newiCodAluno))
                {
                    return BadRequest("Solicitação incorreta");
                }

                var _iCodAlunoGuid = Guid.Parse(iCodAluno);
                
                var alunosEntidade = await _alunoService.GetByIdAluno(KEY_CACHE_ALUNOS, _iCodAlunoGuid);
                var escolaEntidade = await _escolaService.GetAll(KEY_CACHE_ESCOLA);

                if (alunosEntidade == null)
                {
                    _responseAlunosCoreDTO.IsSuccess = false;
                    _responseAlunosCoreDTO.Message = "Não encontramos nenhum resultado.";
                    return Ok(_responseAlunosCoreDTO);
                }

                var retornoJoinEscolaPorUmAluno = await _escolaService.JoinEscolaPorUmAluno(alunosEntidade, escolaEntidade);

                _responseAlunosCoreDTO.IsSuccess = true;
                _responseAlunosCoreDTO.Message = "Sucesso";
                _responseAlunosCoreDTO.objetosAlunos = retornoJoinEscolaPorUmAluno;

                return Ok(_responseAlunosCoreDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro: ", ex.Message);
                return BadRequest("Erro: " + ex.Message);
            }
        }

        [HttpGet]
        [Route("GetFilter")]
        public async Task<IActionResult> GetFilter(string sNome, string sCPF)
        {
            responseAlunosDTO _responseAlunosCoreDTO = new responseAlunosDTO();

            try
            {
                var listaAlunosEntidade = await _alunoService.GetAll(KEY_CACHE_ALUNOS);

                if(listaAlunosEntidade != null)
                {
                    var listaFiltrosAlunos = listaAlunosEntidade.Where(x => x.sNome.ToUpper().Contains(sNome.ToUpper()) || x.sCPF.ToUpper().Contains(sCPF)).ToList();
                    _responseAlunosCoreDTO.listaObjetosAlunos = listaFiltrosAlunos;
                }
                else
                {
                    _responseAlunosCoreDTO.listaObjetosAlunos = null;
                }

                _responseAlunosCoreDTO.IsSuccess = true;
                
                return Ok(_responseAlunosCoreDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro: ", ex.Message);
                return BadRequest("Erro: " + ex.Message);
            }

        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Post(string sNome, DateTime dNascimento, string sCPF, string sEndereco, string sCelular, string iCodEscola)
        {
            responseAlunosDTO _responseAlunosCoreDTO = new responseAlunosDTO();

            try
            {
           
                if (!Guid.TryParse(iCodEscola, out var newiCodEscola))
                {
                    _responseAlunosCoreDTO.IsSuccess = false;
                    _responseAlunosCoreDTO.Message = "Solicitação incorreta";
                    return Ok(_responseAlunosCoreDTO);
                }

                var _iCodEscolaGuid = Guid.Parse(iCodEscola);

                var escolaEntidade = await _escolaService.GetByIdEscola(KEY_CACHE_ESCOLA, _iCodEscolaGuid);

                if (escolaEntidade == null)
                {
                    _responseAlunosCoreDTO.IsSuccess = false;
                    _responseAlunosCoreDTO.Message = "Não foi possível salvar, pois não existe essa escola";
                    return Ok(_responseAlunosCoreDTO);
                }
                
                var listaAlunosEntidade = await _alunoService.GetAll(KEY_CACHE_ALUNOS);

                if (listaAlunosEntidade != null)
                {

                    var existeAluno = listaAlunosEntidade.Where(x => x.sNome.ToUpper() == sNome.ToUpper() || x.sCPF == sCPF).FirstOrDefault();

                    if (existeAluno != null)
                    {
                        _responseAlunosCoreDTO.IsSuccess = false;
                        _responseAlunosCoreDTO.Message = "Não foi possível salvar, pois não existe esse aluno(a)";
                        return Ok(_responseAlunosCoreDTO);
                    }

                }

                Alunos aluno = new();

                aluno.iCodAluno = Guid.NewGuid();
                aluno.sNome = sNome;
                aluno.dNascimento = dNascimento;
                aluno.sCPF = sCPF;
                aluno.sEndereco = sEndereco;
                aluno.sCelular = sCelular;
                aluno.iCodEscola = _iCodEscolaGuid;

                var situacaoAlunosEntidade = await _alunoService.SaveChangesAlunos(KEY_CACHE_ALUNOS, listaAlunosEntidade, aluno);

                if (situacaoAlunosEntidade == false)
                {
                    _responseAlunosCoreDTO.IsSuccess = false;
                    _responseAlunosCoreDTO.Message = "Não foi possível salvar";
                    return Ok(_responseAlunosCoreDTO);
                }
                    
                _responseAlunosCoreDTO.IsSuccess = true;
                _responseAlunosCoreDTO.Message = "Aluno(a): " + aluno.sNome + " - Salvo com sucesso. ";
                
                return Ok(_responseAlunosCoreDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro: ", ex.Message);
                return BadRequest("Erro: " + ex.Message);
            }
            
        }

        [HttpPut]
        [Route("Put")]
        public async Task<IActionResult> Put(string iCodAluno, string sNome = null, DateTime? dNascimento = null, string sCPF = null, string sEndereco = null, string sCelular = null, string iCodEscola = null)
        {
            responseAlunosDTO _responseAlunosCoreDTO = new responseAlunosDTO();

            try
            {
                
                if (!Guid.TryParse(iCodAluno, out var newiCodAluno))
                {
                    _responseAlunosCoreDTO.IsSuccess = false;
                    _responseAlunosCoreDTO.Message = "Solicitação incorreta";
                    return Ok(_responseAlunosCoreDTO);
                }

                if (!Guid.TryParse(iCodEscola, out var newiCodEscola))
                {
                    _responseAlunosCoreDTO.IsSuccess = false;
                    _responseAlunosCoreDTO.Message = "Solicitação incorreta";
                    return Ok(_responseAlunosCoreDTO);
                }

                var _iCodAlunoGuid = Guid.Parse(iCodAluno);
                var _iCodEscolaGuid = Guid.Parse(iCodEscola);

                var listaAlunosEntidade = await _alunoService.GetAll(KEY_CACHE_ALUNOS);
                var alunoEntidade = await _alunoService.GetByIdAluno(KEY_CACHE_ALUNOS, _iCodAlunoGuid);

                var listEscolaEntidade = await _escolaService.GetAll(KEY_CACHE_ESCOLA);
                var escolaEntidade = await _escolaService.GetByIdEscola(KEY_CACHE_ESCOLA, _iCodEscolaGuid);

                if (alunoEntidade == null)
                {
                    _responseAlunosCoreDTO.IsSuccess = false;
                    _responseAlunosCoreDTO.Message = "Não foi possível atualizar, pois não existe esse(a) aluno(a)";
                    return Ok(_responseAlunosCoreDTO);
                }

                if (escolaEntidade == null)
                {
                    _responseAlunosCoreDTO.IsSuccess = false;
                    _responseAlunosCoreDTO.Message = "Não foi possível atualizar, pois não existe essa escola";
                    return Ok(_responseAlunosCoreDTO);
                }

                if (listaAlunosEntidade != null)
                {
                    var existeAluno = listaAlunosEntidade.Where(x => x.sNome.ToUpper() == sNome.ToUpper() || x.sCPF == sCPF).FirstOrDefault();

                    if (existeAluno != null)
                    {
                        _responseAlunosCoreDTO.IsSuccess = false;
                        _responseAlunosCoreDTO.Message = "Não foi possível salvar, pois não existe esse aluno(a)";
                        return Ok(_responseAlunosCoreDTO);
                    }
                }

                Alunos aluno = new();

                aluno.iCodAluno = _iCodAlunoGuid;
                aluno.sNome = string.IsNullOrEmpty(sNome) ? alunoEntidade.sNome : sNome;
                aluno.dNascimento = (!dNascimento.HasValue) ? alunoEntidade.dNascimento : dNascimento.Value ;
                aluno.sCPF = string.IsNullOrEmpty(sCPF) ? alunoEntidade.sCPF : sCPF;
                aluno.sEndereco = string.IsNullOrEmpty(sEndereco) ? alunoEntidade.sEndereco : sEndereco;
                aluno.sCelular = string.IsNullOrEmpty(sCelular) ? alunoEntidade.sCelular : sCelular;
                aluno.iCodEscola = _iCodEscolaGuid;

                var situacaoAlunosEntidade = await _alunoService.UpDateAlunos(KEY_CACHE_ALUNOS, listaAlunosEntidade, aluno);

                if (situacaoAlunosEntidade == false)
                {
                    _responseAlunosCoreDTO.IsSuccess = false;
                    _responseAlunosCoreDTO.Message = "Não foi possível salvar";
                    return Ok(_responseAlunosCoreDTO);
                }

                _responseAlunosCoreDTO.IsSuccess = true;
                _responseAlunosCoreDTO.Message = "Aluno(a): " + aluno.sNome + " - Atualizado com sucesso. ";

                return Ok(_responseAlunosCoreDTO);

            }
            catch (Exception ex)
            {
                _logger.LogError("Erro: ", ex.Message);
                return BadRequest("Erro: " + ex.Message);
            }

        }

        [HttpDelete]
        [Route("Delete")]
        public async Task<IActionResult> Delete(string iCodAluno)
        {
            responseAlunosDTO _responseAlunosCoreDTO = new responseAlunosDTO();

            try
            {
                if (!Guid.TryParse(iCodAluno, out var newiCodAluno))
                {
                    _responseAlunosCoreDTO.IsSuccess = false;
                    _responseAlunosCoreDTO.Message = "Solicitação incorreta";
                    return Ok(_responseAlunosCoreDTO);
                }

                var _iCodAlunoGuid = Guid.Parse(iCodAluno);

                var listaAlunosEntidade = await _alunoService.GetAll(KEY_CACHE_ALUNOS);
                var alunoEntidade = await _alunoService.GetByIdAluno(KEY_CACHE_ALUNOS, _iCodAlunoGuid);

                if (alunoEntidade == null)
                {
                    _responseAlunosCoreDTO.IsSuccess = false;
                    _responseAlunosCoreDTO.Message = "Não foi possível atualizar, pois não existe esse(a) aluno(a)";
                    return Ok(_responseAlunosCoreDTO);
                }

                var alunoExcluido = alunoEntidade.sNome;

                Alunos deletarAluno = new();

                deletarAluno.iCodAluno = _iCodAlunoGuid;
                
                var situacaoAlunosEntidade = await _alunoService.DeleteAlunos(KEY_CACHE_ALUNOS, listaAlunosEntidade, deletarAluno);

                if (situacaoAlunosEntidade == false)
                {
                    _responseAlunosCoreDTO.IsSuccess = false;
                    _responseAlunosCoreDTO.Message = "Não foi possível salvar";
                    return Ok(_responseAlunosCoreDTO);
                }

                _responseAlunosCoreDTO.IsSuccess = true;
                _responseAlunosCoreDTO.Message = "Aluno(a): " + alunoExcluido + " - Excluído com sucesso. ";

                return Ok(_responseAlunosCoreDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro: ", ex.Message);
                return BadRequest("Erro: " + ex.Message);
            }

        }

    }
}
