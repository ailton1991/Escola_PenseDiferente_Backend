﻿using EscolaPenseDiferente.Api.DTO;
using EscolaPenseDiferente.Core.Entities;
using EscolaPenseDiferente.Core.Interfaces.IServices;
using EscolaPenseDiferente.Core.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EscolaPenseDiferente.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EscolaController : ControllerBase
    {

        private const string KEY_CACHE_ALUNOS = "KEY_ALUNOS";
        private const string KEY_CACHE_ESCOLA = "KEY_ESCOLA";

        private readonly IAlunosService _alunoService;
        private readonly IEscolaService _escolaService;
        private readonly ILogger<EscolaController> _logger;

        public EscolaController
        (
            ILogger<EscolaController> logger,
            IAlunosService alunoService,
            IEscolaService escolaService
        )
        {
            _logger = logger;
            _alunoService = alunoService;
            _escolaService = escolaService;
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            responseEscolaDTO _responseEscolaCoreDTO = new responseEscolaDTO();

            try
            {
                var escolaEntidade = await _escolaService.GetAll(KEY_CACHE_ESCOLA);
                var alunosEntidade = await _alunoService.GetAll(KEY_CACHE_ALUNOS);

                if (escolaEntidade == null)
                {
                    _responseEscolaCoreDTO.IsSuccess = false;
                    _responseEscolaCoreDTO.Message = "Não encontramos nenhuma escola.";
                    return Ok(_responseEscolaCoreDTO);
                }

                var retornoJoinAlunosPorEscola = await _escolaService.JoinAlunosPorListaEscolas(escolaEntidade, alunosEntidade);

                _responseEscolaCoreDTO.IsSuccess = true;
                _responseEscolaCoreDTO.Message = "Sucesso";
                _responseEscolaCoreDTO.listaObjetosEscola = retornoJoinAlunosPorEscola;

                return Ok(_responseEscolaCoreDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro: ", ex.Message);
                return BadRequest("Erro: " + ex.Message);
            }
        }

        [HttpGet]
        [Route("GetAllPage")]
        public async Task<IActionResult> GetAllPage(int page, int pageSize)
        {
            responseEscolaDTO _responseEscolaCoreDTO = new responseEscolaDTO();
            try
            {
                var escolaEntidade = await _escolaService.GetAll(KEY_CACHE_ESCOLA, page, pageSize);
                var alunosEntidade = await _alunoService.GetAll(KEY_CACHE_ALUNOS);

                if (escolaEntidade == null)
                    return Ok("Não encontramos nenhum resultado.");

                var retornoJoinAlunosPorEscola = await _escolaService.JoinAlunosPorListaEscolas(escolaEntidade, alunosEntidade);

                _responseEscolaCoreDTO.IsSuccess = true;
                _responseEscolaCoreDTO.Message = "Sucesso";
                _responseEscolaCoreDTO.listaObjetosEscola = retornoJoinAlunosPorEscola;

                return Ok(_responseEscolaCoreDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro: ", ex.Message);
                return BadRequest("Erro: " + ex.Message);
            }
        }

        [HttpGet]
        [Route("GetById")]
        public async Task<IActionResult> GetById(string iCodEscola)
        {
            responseEscolaDTO _responseEscolaCoreDTO = new responseEscolaDTO();
            try
            {
                if (!Guid.TryParse(iCodEscola, out var newiCodEscola))
                {
                    return BadRequest("Solicitação incorreta");
                }

                var _iCodEscolaGuid = Guid.Parse(iCodEscola);

                var escolaEntidade = await _escolaService.GetByIdEscola(KEY_CACHE_ESCOLA, _iCodEscolaGuid);
                var alunosEntidade = await _alunoService.GetAll(KEY_CACHE_ALUNOS);

                if (escolaEntidade == null)
                {
                    _responseEscolaCoreDTO.IsSuccess = false;
                    _responseEscolaCoreDTO.Message = "Não encontramos nenhum resultado.";
                    return Ok(_responseEscolaCoreDTO);
                }

                var retornoJoinAlunosPorEscola = await _escolaService.JoinAlunosPorUmaEscola(escolaEntidade, alunosEntidade);

                _responseEscolaCoreDTO.IsSuccess = true;
                _responseEscolaCoreDTO.Message = "Sucesso";
                _responseEscolaCoreDTO.objetosEscola = escolaEntidade;

                return Ok(_responseEscolaCoreDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro: ", ex.Message);
                return BadRequest("Erro: " + ex.Message);
            }
        }

        [HttpGet]
        [Route("GetFilter")]
        public async Task<IActionResult> GetFilter(string sDescricao)
        {
            responseEscolaDTO _responseEscolaCoreDTO = new responseEscolaDTO();

            try
            {
                var listaEscolasEntidade = await _escolaService.GetAll(KEY_CACHE_ESCOLA);

                if (string.IsNullOrEmpty(sDescricao))
                {
                    _responseEscolaCoreDTO.listaObjetosEscola = listaEscolasEntidade;
                    _responseEscolaCoreDTO.IsSuccess = true;

                    return Ok(_responseEscolaCoreDTO);
                }

                if (listaEscolasEntidade != null)
                {
                    var listaFiltrosEscola = listaEscolasEntidade.Where(x => x.sDescricao.ToUpper().Contains(sDescricao.ToUpper())).ToList();
                    _responseEscolaCoreDTO.listaObjetosEscola = listaFiltrosEscola;
                }
                else
                {
                    _responseEscolaCoreDTO.listaObjetosEscola = null;
                }

                _responseEscolaCoreDTO.IsSuccess = true;
                
                return Ok(_responseEscolaCoreDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro: ", ex.Message);
                return BadRequest("Erro: " + ex.Message);
            }

        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Post(string sDescricao)
        {
            responseEscolaDTO _responseEscolaCoreDTO = new responseEscolaDTO();

            try
            {
                var listEscolaEntidade = await _escolaService.GetAll(KEY_CACHE_ESCOLA);

                if (listEscolaEntidade != null)
                {
                    var existeEscola = listEscolaEntidade.Where(x => x.sDescricao.ToUpper() == sDescricao.ToUpper()).FirstOrDefault();

                    if (existeEscola != null)
                    {
                        _responseEscolaCoreDTO.IsSuccess = false;
                        _responseEscolaCoreDTO.Message = "Não foi possível salvar, pois já existe essa escola.";
                        return Ok(_responseEscolaCoreDTO);
                    }

                }

                Escola escola = new Escola();

                escola.iCodEscola = Guid.NewGuid();
                escola.sDescricao = sDescricao;

                var situacaoEscolaEntidade = await _escolaService.SaveChangesEscola(KEY_CACHE_ESCOLA, listEscolaEntidade, escola);

                if (situacaoEscolaEntidade == false)
                {
                    _responseEscolaCoreDTO.IsSuccess = false;
                    _responseEscolaCoreDTO.Message = "Não foi possível salvar";
                    return Ok(_responseEscolaCoreDTO);
                }

                _responseEscolaCoreDTO.IsSuccess = true;
                _responseEscolaCoreDTO.Message = "Escola: " + sDescricao + " - Salvo com sucesso. ";
                return Ok(_responseEscolaCoreDTO);
                //return Ok("Escola: " + sDescricao + " - Salvo com sucesso. ");
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro: ", ex.Message);
                return BadRequest("Erro: " + ex.Message);
            }

        }

        [HttpPut]
        [Route("Put")]
        public async Task<IActionResult> Put(string iCodEscola, string sDescricao)
        {
            responseEscolaDTO _responseEscolaCoreDTO = new responseEscolaDTO();

            try
            {
                if (!Guid.TryParse(iCodEscola, out var newiCodEscola))
                {
                    return BadRequest("Solicitação incorreta");
                }

                var _iCodEscolaGuid = Guid.Parse(iCodEscola);

                var listEscolaEntidade = await _escolaService.GetAll(KEY_CACHE_ESCOLA);
                var escolaEntidade = await _escolaService.GetByIdEscola(KEY_CACHE_ESCOLA, _iCodEscolaGuid);

                if (escolaEntidade == null)
                {
                    _responseEscolaCoreDTO.IsSuccess = false;
                    _responseEscolaCoreDTO.Message = "Não existe essa escola";
                    return Ok(_responseEscolaCoreDTO);
                }

                Escola atualizacaoEscola = new Escola();

                atualizacaoEscola.iCodEscola = escolaEntidade.iCodEscola;
                atualizacaoEscola.sDescricao = sDescricao;

                var situacaoEscolaEntidade = await _escolaService.UpDateEscola(KEY_CACHE_ESCOLA, listEscolaEntidade, atualizacaoEscola);

                if (situacaoEscolaEntidade == false)
                {
                    _responseEscolaCoreDTO.IsSuccess = false;
                    _responseEscolaCoreDTO.Message = "Não foi possível atualizar.";
                    return Ok(_responseEscolaCoreDTO);
                }

                _responseEscolaCoreDTO.IsSuccess = true;
                _responseEscolaCoreDTO.Message = "Escola: " + sDescricao + " - Atualizado com sucesso. ";
                return Ok(_responseEscolaCoreDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro: ", ex.Message);
                return BadRequest("Erro: " + ex.Message);
            }

        }

        [HttpDelete]
        [Route("Delete")]
        public async Task<IActionResult> Delete(string iCodEscola)
        {
            responseEscolaDTO _responseEscolaCoreDTO = new responseEscolaDTO();

            try
            {
                if (!Guid.TryParse(iCodEscola, out var newiCodEscola))
                {
                    return BadRequest("Solicitação incorreta");
                }

                var _iCodEscolaGuid = Guid.Parse(iCodEscola);

                var listEscolaEntidade = await _escolaService.GetAll(KEY_CACHE_ESCOLA);
                var escolaEntidade = await _escolaService.GetByIdEscola(KEY_CACHE_ESCOLA, _iCodEscolaGuid);
                var alunosEntidade = await _alunoService.GetAll(KEY_CACHE_ALUNOS);

                if (escolaEntidade == null)
                {
                    _responseEscolaCoreDTO.IsSuccess = false;
                    _responseEscolaCoreDTO.Message = "Não existe essa escola";
                    return Ok(_responseEscolaCoreDTO);
                }

                var retornoJoinAlunosPorEscola = await _escolaService.JoinAlunosPorUmaEscola(escolaEntidade, alunosEntidade);

                if(retornoJoinAlunosPorEscola.Alunos.Count > 0)
                {
                    _responseEscolaCoreDTO.IsSuccess = false;
                    _responseEscolaCoreDTO.Message = "Não pode ser excluído, pois existe alunos vinculados.";
                    return Ok(_responseEscolaCoreDTO);
                }

                var escolaExcluida = escolaEntidade.sDescricao;

                Escola deletarEscola = new Escola();

                deletarEscola.iCodEscola = escolaEntidade.iCodEscola;

                var situacaoEscolaEntidade = await _escolaService.DeleteEscola(KEY_CACHE_ESCOLA, listEscolaEntidade, deletarEscola);

                if (situacaoEscolaEntidade == false)
                {
                    _responseEscolaCoreDTO.IsSuccess = false;
                    _responseEscolaCoreDTO.Message = "Não foi possível deletar.";
                    return Ok(_responseEscolaCoreDTO);
                }

                _responseEscolaCoreDTO.IsSuccess = true;
                _responseEscolaCoreDTO.Message = "Escola: " + escolaExcluida + " - Excluído com sucesso. ";
                return Ok(_responseEscolaCoreDTO);

            }
            catch (Exception ex)
            {
                _logger.LogError("Erro: ", ex.Message);
                return BadRequest("Erro: " + ex.Message);
            }

        }
    }
}
