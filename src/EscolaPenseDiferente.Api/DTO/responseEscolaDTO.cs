﻿using EscolaPenseDiferente.Core.Entities;

namespace EscolaPenseDiferente.Api.DTO
{
    public class responseEscolaDTO
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string MessageConsole { get; set; }
        public Escola objetosEscola { get; set; }
        public List<Escola> listaObjetosEscola { get; set; }
    }
}
