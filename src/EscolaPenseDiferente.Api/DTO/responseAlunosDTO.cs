﻿using EscolaPenseDiferente.Core.Entities;

namespace EscolaPenseDiferente.Api.DTO
{
    public class responseAlunosDTO
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string MessageConsole { get; set; }
        public Alunos objetosAlunos { get; set; }
        public List<Alunos> listaObjetosAlunos { get; set; }
    }
}
