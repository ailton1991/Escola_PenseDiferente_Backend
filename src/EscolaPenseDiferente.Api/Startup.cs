﻿using EscolaPenseDiferente.Infra.InversionOfControl;
using Microsoft.OpenApi.Models;

namespace EscolaPenseDiferente.Api
{
    public class Startup
    {

        //private const string CONEXAO_ESCOLA_PENSE_DIFERENTE = "CONEXAO_ESCOLA_PENSE_DIFERENTE";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();

            services.AddControllers();

            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Escola Pense Diferente", Version = "v1" });
            });

            services.AddMemoryCache();

            //string conexaoEscolaPenseDiferente = Configuration.GetConnectionString(CONEXAO_ESCOLA_PENSE_DIFERENTE);

            //services.AddDbContext<EscolaPenseDiferenteContext>(options => options.UseSqlServer(conexaoEscolaPenseDiferente));

            services.startService();// Inversion of Control - Juntei todos os 'Service' em um único lugar.

            services.startRepository();// Inversion of Control - Juntei todos os 'Repository' em um único lugar.
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseCors("MyPolicy");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

    }
}
