﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscolaPenseDiferente.Core.Entities
{
    public class Escola
    {
        public Escola()
        {
            Alunos = new List<Alunos>();
        }

        [Key]
        public Guid iCodEscola {  get; set; }
        public string sDescricao { get; set; }

        [NotMapped]
        public List<Alunos> Alunos { get; set; }

    }
}
