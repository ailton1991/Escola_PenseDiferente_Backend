﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscolaPenseDiferente.Core.Entities
{
    public class Alunos
    {
        public Alunos()
        {
            
        }

        [Key]
        public Guid iCodAluno { get; set; }
        public string sNome { get; set; }
        public DateTime dNascimento { get; set; }
        public string sCPF { get; set; }
        public string sEndereco { get; set; }
        public string sCelular { get; set; }
        public Guid iCodEscola { get; set; }

        [NotMapped]
        public virtual Escola Escola { get; set; }

    }
}
