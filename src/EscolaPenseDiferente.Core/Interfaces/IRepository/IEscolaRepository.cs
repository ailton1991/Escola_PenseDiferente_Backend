﻿using EscolaPenseDiferente.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscolaPenseDiferente.Core.Interfaces.IRepository
{
    public interface IEscolaRepository : IBaseRepository<Escola> { }
}
