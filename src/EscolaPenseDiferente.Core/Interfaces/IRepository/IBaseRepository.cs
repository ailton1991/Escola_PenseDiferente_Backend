﻿using EscolaPenseDiferente.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscolaPenseDiferente.Core.Interfaces.IRepository
{
    public interface IBaseRepository<T> where T : class
    {

        Task<List<T>> GetAll(string KEY_CACHE);
        Task<List<T>> GetAll(string KEY_CACHE, int page, int pageSize);
        Task<Alunos> GetByIdAluno(string KEY_ALUNO, Guid id);
        Task<Escola> GetByIdEscola(string KEY_ESCOLA, Guid id);
        Task<bool> SaveChangesAlunos(string KEY_ALUNOS, List<Alunos> listAlunos, Alunos newAluno);
        Task<bool> SaveChangesEscola(string KEY_ESCOLA, List<Escola> listEscola, Escola newEscola);
        Task<bool> UpDateAlunos(string KEY_ALUNOS, List<Alunos> listAlunos, Alunos updateAlunos);
        Task<bool> UpDateEscola(string KEY_ESCOLA, List<Escola> listEscola, Escola updateEscola);
        Task<bool> DeleteAlunos(string KEY_ALUNOS, List<Alunos> listAlunos, Alunos deleteAlunos);
        Task<bool> DeleteEscola(string KEY_ESCOLA, List<Escola> listEscola, Escola deleteEscola);
        
    }
}
