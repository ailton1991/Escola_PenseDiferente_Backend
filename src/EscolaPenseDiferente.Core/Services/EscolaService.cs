﻿using EscolaPenseDiferente.Core.Entities;
using EscolaPenseDiferente.Core.Interfaces.IRepository;
using EscolaPenseDiferente.Core.Interfaces.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscolaPenseDiferente.Core.Services
{
    public class EscolaService : BaseService<Escola>, IEscolaService
    {
        public EscolaService(IEscolaRepository repository) : base(repository)
        {
                
        }
    }
}
