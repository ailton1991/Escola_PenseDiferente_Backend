﻿using EscolaPenseDiferente.Core.Entities;
using EscolaPenseDiferente.Core.Interfaces.IRepository;
using EscolaPenseDiferente.Core.Interfaces.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscolaPenseDiferente.Core.Services
{
    public class BaseService<T> : IBaseService<T> where T : class
    {
        private IBaseRepository<T> _repository;

        public BaseService(IBaseRepository<T> repository)
        {
            _repository = repository;
        }

        public Task<List<T>> GetAll(string KEY_CACHE)
        {
            return _repository.GetAll(KEY_CACHE);
        }

        public Task<List<T>> GetAll(string KEY_CACHE, int page, int pageSize)
        {
            return _repository.GetAll(KEY_CACHE, page, pageSize);
        }

        public Task<Alunos> GetByIdAluno(string KEY_ALUNO, Guid id)
        {
            return _repository.GetByIdAluno(KEY_ALUNO, id);
        }

        public Task<Escola> GetByIdEscola(string KEY_ESCOLA, Guid id)
        {
            return _repository.GetByIdEscola(KEY_ESCOLA, id);
        }

        public Task<bool> SaveChangesAlunos(string KEY_ALUNOS, List<Alunos> listAlunos, Alunos newAluno)
        {
            return _repository.SaveChangesAlunos(KEY_ALUNOS, listAlunos, newAluno);
        }

        public Task<bool> SaveChangesEscola(string KEY_ESCOLA, List<Escola> listEscola, Escola newEscola)
        {
            return _repository.SaveChangesEscola(KEY_ESCOLA, listEscola, newEscola);
        }

        public async Task<bool> UpDateAlunos(string KEY_ALUNOS, List<Alunos> listAlunos, Alunos updateAlunos)
        {
            return await _repository.UpDateAlunos(KEY_ALUNOS, listAlunos, updateAlunos);
        }

        public async Task<bool> UpDateEscola(string KEY_ESCOLA, List<Escola> listEscola, Escola updateEscola)
        {
            return await _repository.UpDateEscola(KEY_ESCOLA, listEscola, updateEscola);
        }

        public Task<bool> DeleteAlunos(string KEY_ALUNOS, List<Alunos> listAlunos, Alunos deleteAlunos)
        {
            return _repository.DeleteAlunos(KEY_ALUNOS, listAlunos, deleteAlunos);
        }

        public Task<bool> DeleteEscola(string KEY_ESCOLA, List<Escola> listEscola, Escola deleteEscola)
        {
            return _repository.DeleteEscola(KEY_ESCOLA, listEscola, deleteEscola);
        }

        public async Task<List<Escola>> JoinAlunosPorListaEscolas(List<Escola> escolaEntidade, List<Alunos> alunosEntidade)
        {

            List<Escola> listaEscola = new List<Escola>();

            if (alunosEntidade != null)
            {

                foreach (var cadaEscola in escolaEntidade)
                {
                    Escola escola = new Escola();
                    escola.Alunos = new List<Alunos>();
                    cadaEscola.Alunos = new List<Alunos>();

                    var alunosPorEscola = alunosEntidade.Where(x => x.iCodEscola == cadaEscola.iCodEscola).ToList();

                    if (alunosPorEscola != null)
                    {
                        escola = cadaEscola;
                        escola.Alunos.AddRange(alunosPorEscola);
                        listaEscola.Add(escola);
                    }
                }

            }
            else
            {
                listaEscola = escolaEntidade;
            }

            return listaEscola;
        }

        public async Task<Escola> JoinAlunosPorUmaEscola(Escola escolaEntidade, List<Alunos> alunosEntidade)
        {
            Escola escola = new Escola();

            if (alunosEntidade != null)
            {
                escola.Alunos = new List<Alunos>();
                escolaEntidade.Alunos = new List<Alunos>();

                var alunosPorEscola = alunosEntidade.Where(x => x.iCodEscola == escolaEntidade.iCodEscola).ToList();

                if (alunosPorEscola != null)
                {
                    escola = escolaEntidade;
                    escola.Alunos.AddRange(alunosPorEscola);
                }

            }
            else
            {
                escola = escolaEntidade;
            }

            return escola;
        }


        public async Task<List<Alunos>> JoinEscolaPorListaAluno(List<Alunos> alunosEntidade, List<Escola> escolaEntidade)
        {
            List<Alunos> listaAlunos = new List<Alunos>();

            if (alunosEntidade != null)
            {
                foreach (var cadaAluno in alunosEntidade)
                {
                    Alunos aluno = new Alunos();
                    aluno.Escola = new();

                    var escolaPorAluno = escolaEntidade.Where(x => x.iCodEscola == cadaAluno.iCodEscola).FirstOrDefault();
                    aluno = cadaAluno;
                    aluno.Escola = escolaPorAluno;

                    listaAlunos.Add(aluno);
                }

                return listaAlunos;
            }
            else
            {
                return listaAlunos;
            }

        }

        public async Task<Alunos> JoinEscolaPorUmAluno(Alunos alunosEntidade, List<Escola> escolaEntidade)
        {
            Alunos aluno = new Alunos();

            if (alunosEntidade != null)
            {
                aluno.Escola = new();

                var escolaPorAluno = escolaEntidade.Where(x => x.iCodEscola == alunosEntidade.iCodEscola).FirstOrDefault();
                aluno = alunosEntidade;
                aluno.Escola = escolaPorAluno;

                return aluno;
            }
            else
            {
                return aluno;
            }
        }
    }
}
