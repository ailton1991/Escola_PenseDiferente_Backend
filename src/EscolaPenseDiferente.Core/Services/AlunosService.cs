﻿using EscolaPenseDiferente.Core.Entities;
using EscolaPenseDiferente.Core.Interfaces.IRepository;
using EscolaPenseDiferente.Core.Interfaces.IServices;
using Microsoft.Extensions.Logging;

namespace EscolaPenseDiferente.Core.Services
{
    public class AlunosService : BaseService<Alunos>, IAlunosService
    {
        private readonly IAlunosRepository _repository;
        private readonly ILogger<AlunosService> _logger;
        public AlunosService(IAlunosRepository repository, ILogger<AlunosService> logger) : base(repository)
        {
            _repository = repository;
            _logger = logger;
        }
    }
}
